
import urllib.request
from bs4 import BeautifulSoup
import jinja2


url = 'https://www.nytimes.com/2017/10/29/business/virtual-reality-driverless-cars.html'
url_reader = urllib.request.urlopen(url).read().decode('utf-8')
html_soup = BeautifulSoup(url_reader, 'html.parser')


urllib.request.urlretrieve('https://static01.nyt.com/images/2017/10/28/business/30VIRTUALROADS-3/30VIRTUALROADS-3-master675.jpg', 'nytimes_webscrape_img_1')
urllib.request.urlretrieve('https://static01.nyt.com/images/2017/10/28/business/30VIRTUALROADS-4/30VIRTUALROADS-4-master675.jpg', 'nytimes_webscrape_img_2')

images = ['nytimes_webscrape_img_1', 'nytimes_webscrape_img_2']


title = html_soup.title.contents[0]
byline = html_soup.find('span', {'itemprop': "name"}).get_text()
article_date = html_soup.find('time', {'class': "css-129k401 e16638kd0"}).get_text()

article_text = html_soup.find_all('p', {'class': "css-exrw3m evys1bk0"})
paragraphs = [ptag.get_text() for ptag in article_text]


template_dir = '/Users/Mikolaj/Downloads'
env = jinja2.Environment()
env.loader = jinja2.FileSystemLoader(template_dir)
template = env.get_template('story_template.html')
completed_page = template.render(title=title, author=byline, date=article_date, paragraphs=paragraphs, images=images)


with open ('ny_times_scrape.html','w') as file_writer:
	file_writer.write(completed_page)